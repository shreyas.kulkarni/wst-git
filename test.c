#include <stdio.h>
#include "integer.h" 
#include <stdlib.h>
int main() {
    Integer a, b, c;
    char ch;
    char str[64]; 
    initInteger(&a);
    initInteger(&b);
    while((ch = getchar()) != '\n')
        addDigit(&a, ch); 
    scanf("%s", str);
    b = createIntegerFromString(str);
    c = addIntegers(a, b); 
    printInteger(c);
    destroyInteger(&c);
    c = substractIntegers(a, b);
    printInteger(c);
    destroyInteger(&c);
}
