#include "integer.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

void initInteger(Integer *a) {
	a->head = NULL;
	a->tail = NULL;
}

void addDigit(Integer *a, char c) {
    	node *tmp;
    	if(isdigit(c)) {
        tmp = (node*)malloc(sizeof(node));
        tmp->chr = c;
        tmp->nxt = NULL;
        	if(a->head == NULL) {
            		tmp->pre = NULL;
            		a->head = tmp;
            		a->tail = tmp;
            		a->len = a->len + 1;
        	}
        	else {
            		a->tail->nxt = tmp;
            		tmp->pre = a->tail;  
            		a->tail = tmp;
            		a->len = a->len + 1;
        	}
    	}
    	else {
    	}  
}


Integer createIntegerFromString(char *str) {
	Integer a;
	initInteger(&a);
	int i, l;
	node *tp;
	l = strlen(str);
	for(i = 0; i < l; i++) {
		tp = (node*)malloc(sizeof(char));
		if(str[i] >= '0' && str[i] <= '9') {
			tp->chr = str[i];
			if(a.head == NULL) {
				tp->nxt = tp->pre = NULL;
				a.head = a.tail = tp;
			}
			tp->pre = a.tail;
			a.tail->nxt = tp;
			a.tail = a.tail->nxt;
			a.tail->nxt = NULL;
			a.len = a.len + 1;
		}
		else {
			break;
		}
	}

	if(a.len == 0) {
		tp->chr = '0';
		tp->nxt = tp->pre = NULL;
		a.head = a.tail = tp;
		a.len = a.len + 1;
	}
	return a;
}

int lgth(Integer a) {
	int c = 1;
	while(a.head->nxt != NULL) {
		c++;
		a.head = a.head->nxt;
	}
	return c;	
}


void printInteger(Integer a) {
	node *pt;
	pt = a.head;
	int x, len, i;
	len = lgth(a);
	for(i = 0; i < len; i++){
		x = a.head->chr - '0';
		if(x < 0) {
			do {
				a.head->chr = '0';
				a.head = a.head->nxt;
			} while(a.head->nxt != NULL);
			
			a.head->chr = '0';
		}	
	}
	printf("\n[");
	while(pt->nxt != NULL) {
		printf("%c", pt->chr);
		pt = pt->nxt;
	}
	printf("%c", pt->chr);
	printf("]");
}

Integer addIntegers(Integer a, Integer b) {
	short no1, no2, carry = 0, x;
	char str;
	Integer res;
	initInteger(&res);
	node *p, *q, *tmp;
	p = a.tail;
	q = b.tail;
	int lcnt = 0;
	if(a.len < b.len) {
		while(a.len < b.len) {
			tmp = (node *)malloc(sizeof(node));
			tmp->chr = '0';
			tmp->nxt = a.head;
			a.head->pre = tmp;
			a.head = tmp;
			a.head->pre = NULL;
			a.len++;
		}
	}
	else if(a.len >= b.len) {
		while(a.len > b.len) {
			tmp = (node *)malloc(sizeof(node));
			tmp->chr = '0';
			tmp->nxt = b.head;
			b.head->pre = tmp;
			b.head = tmp;
			b.head->pre = NULL;
			b.len++;
		}
	}
	
	if(a.len == 1 && b.len == 1) {
		tmp = (node *)malloc(sizeof(node));
		tmp->chr = '0';
		tmp->nxt = a.head;
		a.head->pre = tmp;
		tmp->pre = NULL;
		a.head = tmp;
		a.len++;
		
		tmp = (node *)malloc(sizeof(node));
		tmp->chr = '0';
		tmp->nxt = b.head;
		b.head->pre = tmp;
		tmp->pre = NULL;
		b.head = tmp;
	}
	
	
	while(lcnt <= a.len) {
		tmp = (node *)malloc(sizeof(node));
		if(p->chr == '-' && q->chr == '-')
			break;
		no1 = p->chr - '0';
		no2 = q->chr - '0';
		x = no1 + no2 + carry;
		carry = 0;
		if(x > 9) {
			sprintf(&str, "%d", (x % 10));
			carry = x / 10;
		}
		else {
			sprintf(&str, "%d", x);
			carry = 0;	
		}
		tmp->chr = str;
		if(res.head == NULL && res.tail == NULL) {
			res.head = res.tail = tmp;
			res.tail->nxt = res.tail->pre = NULL;
			res.len++;
			p = p->pre;
			q = q->pre;
			continue;
		}
		lcnt++;
		tmp->nxt = res.head;
		res.head->pre = tmp;
		res.head = tmp;
		res.head->pre = NULL;
		if((lcnt == a.len - 1)) {
			if(carry == 0)
				break;
			else {
				tmp = (node *)malloc(sizeof(node));
				sprintf(&(tmp->chr), "%d", carry);
				tmp->nxt = res.head;
				res.head->pre = tmp;
				res.head = tmp;
				res.head->pre = NULL;
				res.len++;
				break;
			}
				
		}
		p = p->pre;
		q = q->pre;
		res.len += 2;
	}
	return res;
}

Integer substractIntegers(Integer a, Integer b) {	
	short no1, no2, x;
	char str;
	int borrow = 0;
	Integer sub;
	initInteger(&sub);
	Integer res;
	initInteger(&res);
	node *p, *q, *tmp;
	p = a.tail;
	q = b.tail;
	int lcnt = 0;
	if(a.len < b.len) {
		while(a.len < b.len) {
			tmp = (node *)malloc(sizeof(node));
			tmp->chr = '0';
			tmp->nxt = a.head;
			a.head->pre = tmp;
			a.head = tmp;
			a.head->pre = NULL;
			a.len++;
		}
	}
	else if(a.len > b.len) {
		while(a.len > b.len) {
			tmp = (node *)malloc(sizeof(node));
			tmp->chr = '0';
			tmp->nxt = b.head;
			b.head->pre = tmp;
			b.head = tmp;
			b.head->pre = NULL;
			b.len++;
		}
	}
	
	while(lcnt < a.len) {
		tmp = (node *)malloc(sizeof(node));
		if(p->chr == '-' && q->chr == '-')
			break;
		no1 = p->chr - '0';
		no2 = q->chr - '0';
		if(borrow) {
			no1--;
			borrow = 0;
		}
		if(no1 < no2) {
			if(p->pre != NULL) {
				borrow = 1;
				no1 = no1 + 10;
			}
		}
		x = no1 - no2;
	
		sprintf(&str, "%d", x);
		lcnt++;						
		tmp->chr = str;
	 	if(sub.head == NULL && sub.tail == NULL) {
			sub.head = sub.tail = tmp;
			sub.tail->nxt = sub.tail->pre = NULL;
			p = p->pre;
			q = q->pre;
			continue;
		}
		sub.len++;
		tmp->nxt = sub.head;
		sub.head->pre = tmp;
		sub.head = tmp;
		sub.head->pre = NULL;
		p = p->pre;
		q = q->pre;
	}	
	return sub;	 
}

void destroyInteger(Integer *a) {
    node *tmp;
    if(a->head == NULL)
        return;
    while(a->head->next != NULL) {
        tmp = a->head;
        a->head = a->head->nxt;
        free(tmp);
    }
    tmp = a->head;
    a->head = NULL;
    a->tail = NULL;
    free(tmp);
}
